# Command line and Environment #

## Command line options ##
    --f=configfile         Configuration filename. Defaults to "foopipes.json, or foopipes.yaml, or foopipes.yml"
    --m=moduledir          Module directory. Defaults to "modules"
    --p=plugindir          Plugins directory
    --scheduler=[on|off]   Scheduler enabled/disabled. Defaults to "on" 
    --verbose=[on|off]     Super verbose debug logging on/off. Defaults to "off"
    --telemetry=[on|off]   Send anonymous encrypted telemetry information on/off. Defaults to "on".
    run <input> <topic> <filename> Run pipeline by triggering _input_ with topic _topic_ with json contents of file _filename_
    config                 Send default foopipes.yml to standard output.

### Environment variables ###
The following environment variables are used.

     FOOPIPES_configfile Configuration filename.
     FOOPIPES_moduledir  Module directory.
     FOOPIPES_scheduler  Scheduler enabled/disabled.

Environment variables are substituted in config files using the syntax ${VARIABLE}. 
Default values can be used by using the shell syntax ${VARIABLE:-DEFAULTVALUE}. 
Variable names are not case sensitive.

```
#!yaml
    url: "http://${MyService}/api",
    secondUrl: "http://${MyOtherService:-localhost}/api"
```

### Anonymous Telemetry Data ###

Anonymous telemetry is enabled by default. Telemetry can be disabled with command line option --telemetry=off.

Telemetry data is sent encrypted over UDP and consists of a random uuid, fatal exceptions and pipeline data count.

No personal data is collected.
