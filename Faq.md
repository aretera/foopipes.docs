# FAQ #

## What is it? ##
This is the most common question and still the hardest one to answer. We've tried to answer this elsewhere on this website, but in short, after doing hundreds of programs moving and transforming data between domains we thought it could be generalized. This is an attempt to build a production ready service that could be run on premises, as a service, or "serverless" if you'd like, that would work for 80% of the most common scenarios with no or very little programming. 

The original idea was to build something that is a mix of Logstash and AWS Lambda. Not sure if that comparison still holds.

The asynchronous nature of Foopipes has made it obvious it can also be used for orchestrating asynchronous workflows and not only data pipelining. The real strength appears when it is used in a combination of both.

## What is it not? ##
Foopipes is not a heavyweight integration engine like Biztalk. 

It is not an API gateway.

It is not a platform.

## What data formats does it support? ##
Currenly it is completely based on Json but can also parse XML. Other formats may follow such Protobuf. It can also handle binary blobs which makes it possible to do custom logic on the data by using Node.js modules or Plugins. Image processing an example of this.

## Does it support XX ? ##
Foopipes is based on Plugins for popular services such as queue services and data storages. You can also write your own plugins, or it is often possible to write what's needed as Node.js modules.

Contact us and we can help you.

## What does _foo_ mean? ##

In programming, _foo_ is commonly used as a placeholder variable without meaning. 

"The terms foobar (/ˈfuːbɑːr/), or foo and others are used as placeholder names (also referred to as metasyntactic variables) in computer programming or computer-related documentation. They have been used to name entities such as variables, functions, and commands whose exact identity is unimportant and serve only to demonstrate a concept." -- [Wikipedia](https://en.wikipedia.org/wiki/Foobar)

"Approximately 212 RFCs, or about 7% of RFCs issued so far, starting with [RFC269], contain the terms 'foo', 'bar', or 'foobar' used as a metasyntactic variable without any proper explanation or definition." -- [RFC3092](https://tools.ietf.org/html/rfc3092)

## Is source code available? ##
Foopipes is currently not open source but if your corporate policy requires the source code to be available it can be arranged. Please contact us. 

## Is Docker required? ##
For the free community edition, currently yes. Otherwise please contact us and we'll arrange something.

## Why Typescript? ##
Typescript and promises was gods gift to Javascript, but now es6 Javascript is almost as good. You can use either.
