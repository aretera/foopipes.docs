# Getting Started #

## Running ##
Foopipes runs in Docker as a Linux container.

* Install Docker. https://www.docker.com/
* Open a terminal and create a default yaml configuration file and name it `foopipes.yml`.
```
docker run aretera/foopipes config >foopipes.yml
```
* Edit `foopipes.yml` to suit your needs.
* Start Foopipes as a Docker container and mount the current directory as `/project`.
```
docker run -v $(pwd):/project aretera/foopipes
```
Windows:
```
docker run -v %CD%:/project aretera/foopipes
```

## Concepts ##

### Services ###
_Services_ is a definition of an interface to a system. Each service has a _type_ which is the actual implementation of the service.

All services has a set of _capabilities_ which dictates how you can interact with the service. Services are built in into Foopipes, or third party implementations that are loaded as plugins.

Common capabilities are:
* Queue 
* Key value storage
* Indexed storage
* Event emitter


### Pipelines ###

Pipelines is a set of _tasks_ that will happen once a pipeline has been triggered. A pipeline is triggered by a service which is capable of emitting events, such as the _queue_ service, _httplistener_ service, or the _scheduler_ service.

### Tasks ###
Tasks is what are executed in a pipeline. Often a task operates against one or more _services_. For a task to work with a specific service it is required that the service has the capabilities the task requires. For instance, the _publish_ task requires the specified service has the _queue_ capability.

### Plugins ###
Plugins are third party extensions Foopipes. Commonly a plugin are _services_ and/or a set of _tasks_. Plugins are loaded at startup.
