# Pipelines #

```
pipelines:
  -
    when:
    from:
    do:
    to:
    then:
    error:
    finally:
```

## when ##
What triggers this pipeline. This is what services this pipeline subscribes to.
```
    when:
      - { service: queue, topic: something }
```

## from ##
This is typically where the input data is fetched for processing in the _do_ section. If the _from_ section is missing or empty the input data is what triggered the pipeline in the the _when_ section.

```
    from:
      - { task: enumKeys, service: myfiles }
```
_Note_ Although possible, currenly there is no point to specify more than one _from_ task. Future features of the product could be joins from different sources.

## do ##
Here you _do_ things with the data. All output from each _do_ task cumulated and passed on to the next _do_ task. 

```
    do:
     - { task: node, module: mymodule, function: somefunc }
```
## to ##
This is where you typically store the outcome to a storage. All _to_ tasks are invoked in parallel with the same data, the cumulated output from the last _do_ task.

```
    to:
     - { task: store, service: myfiles }
     - { task: publish, service: queue, topic: processthis }
```

## then ##
_Then_ tasks are executed once when all tasks where successful. Aborted tasks are still considered as successful.

You can constraint the tasks in this section by using ```empty: true/false``` or ```aborted: true/false``` on the task.
```
    then: 
       - { task: publish, service: queue, topic: gotempty, empty: true, aborted: false }
       - { task: publish, service: queue, topic: something, empty: false, aborted: false }
       - { task: publish, service: queue, topic: regardless }
```

## error ##
These tasks are invoked in case any task threw an error. Aborted tasks aren't considered as an error.
```
    error:
       - { task: node, module: mymodule, function: reportError }
```

## finally ##
Always executed regardless of error, aborted or success.
```
    finally:
      - { task: publish, service: queue, topic: proceed_to_next_step }
```

## id ##
All pipelines have a generated id unless explic specified.
```
pipelines:
  -
    id: mypipeline
    when:
       ...
```

## semaphore ##
The number of simultaneous executions of a pipeline can be limited with the semaphore option.

```
pipelines:
  -
    id: mypipeline
    semaphore: 1
    when:
       ...
```

