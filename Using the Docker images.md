# Using the Docker images #

Foopipes runs in Docker as a Linux container.

## Creating default config ##

You can use the Docker image to create a default configuration file that can be used as a starting point for your project.

```
docker run --rm aretera/foopipes config >foopipes.yml
```

## Running ##
Mount current directory as `/project` and start Foopipes. 

```
docker run -v $(pwd):/project aretera/foopipes:latest-sdk
```
for Windows, replace `$(pwd)` with `%CD%` for an absolute path to current directory.

```
docker run -v %CD%:/project aretera/foopipes:latest-sdk
```

### Exposing ports ###
For exposing the listener port use the `-p`option.

```
docker run -v $(pwd):/project -p 80:80 aretera/foopipes:latest-sdk
```

## SDK images ##
There are two variants of the Foopipes Docker images, one runtime only variant and one _SDK_ variant with a `-sdk` suffix. The _SDK_ variant contains Typescript and runs `npm install` and `tsc` before starting Foopipes. This is useful if you don't want these tools installed on the host and don't want to commit the build output into your source repository.
