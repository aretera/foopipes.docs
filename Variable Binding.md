# Variable Binding #

Arguments to tasks in the configuration can be bound to runtime variables in the format:
    
    #{[namespace:]<variable>[:-defaultValue]}

```
      - { task: httpget, url: "#{image.url}" } 
```
```
      - { task: httpget, url: "#{myStore:myurl}" } 
```

## Namespaces ##
Namespaces are either one of the built in namespaces or a reference to one of the configured services when supported by the service.

If the namespace is ommited, the first available result is returned, evaluated in the following order: `data`, `metadata`, `config`. 

### Built in namespaces ###
    data
    metadata
    config
    env
   
### Services namespaces ###
If a configured service has the capability of acting like a key-value store, it can be referenced for use in task arguments. The variable name is the key used 
for doing the lookup.
```
#!yaml
    do:
      - { task: fetchUpdates, token: "#{elasticsearch:lastUpdateToken}", outToken: "elasticsearch:lastUpdateToken" } 
```

## Variables  ##
The variable may be a Json path specifier, or a key in a keyvalue dictionary.

## Default values ##
It is possible to provide inline default values using typical shell syntax. Currently, the contents of the default value is used as is and not expanded.

* `#{VARIABLE:-default}` will evaluate to default if VARIABLE is unset or empty.
* `#{VARIABLE-default}` will evaluate to default only if VARIABLE is unset.

## Examples ##
```
#!yaml
    -
      when:
        - { service: queue, topic: entry }
      from:
        - { task: fetchEntry, id: "#{sys.id}", spaceId: "#{sys.space.sys.id}" }
      to: 
        - { task: store, service: elasticsearch, index: entries, key: "#{sys.id}" }
```