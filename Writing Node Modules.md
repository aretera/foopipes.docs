# Writing Node Modules #

You can write Node.js modules in pure Javascript or Typescript 2.

If you use a Dockerimage with -sdk suffix your .ts files are compiled at startup.

### Plain Javascript Examples ###
Create a file ```modules/mymodule.js```

```
function myFunction(event, context) {
    console.log("Hello world");
    event.myfield = "hello world";
    return event;
}

function myPromiseFunction(event, context) {
    return new Promise(function (resolve, reject) {
        event.myfield = "hello promised world";
        resolve(event);
    });
}
exports.myFunction = myFunction;
exports.myPromiseFunction = myPromiseFunction;
```

Invoke with:
```
  do:
     - { node, module: mymodule, function: myFunction }
     - { node, module: mymodule, function: myPromiseFunction }
```

### Typescript Examples ###
Create a file ```modules_src/mymodule.ts```

```
import * as foopipes from "foopipes";
import ICallContext = foopipes.ICallContext;
import IResult = foopipes.IResult;
import ProcessJsonResult = foopipes.ProcessJsonResult;
import ProcessStreamResult = foopipes.ProcessStreamResult;

export function NoPromiseTest(event: any, context: ICallContext): Object {
    return event;
}

export function PromiseTest(event: any, context: ICallContext): Promise<IResult> {
    return new Promise((resolve, reject) => {
        resolve(event);
    });
}

export function StreamWithoutEndTest(event: any, context: ICallContext): Promise<IResult> {
    return new Promise<IResult>((resolve, reject) => {
        const r = new ProcessStreamResult(context);
        r.stream.write("hello", "utf8", ()=> { resolve(r);return true;});
    });
}

export async function AsyncTest(event: any, context: ICallContext): Promise<IResult> {
    var r = await foopipes.bindValue(context, "#{file:myvalue}");
    return new ProcessJsonResult(event);
}

```

## Callbacks to Foopipes ##
Use the latest Foopipes NPM module for Typescript types. The actual implementation is injected at runtime.

```
function publish(context: ICallContext, topic: string, data: Object) : Promise<void> 
function publishAndWait(context: ICallContext, topic: string, data: Object) : Promise<void> 
function setValue(context: ICallContext, key: string, value: string): Promise<void> 
function bindValue(context: ICallContext, bindingExpression: string) : Promise<string>
function load(context: ICallContext, serviceName: string, index: string, dataType: string, key: string): Promise<any> 
function store(context: ICallContext, serviceName: string, index: string, dataType: string, key: string, data: Object) : Promise<void> 
function storeDelayed(context: ICallContext, serviceName: string, index: string, dataType: string, key: string, data: Object): Promise<void> 
function deleteObject(context: ICallContext, serviceName: string, index: string, dataType: string, key: string) : Promise<void> 
function enumKeys(context: ICallContext, serviceName: string, index: string, dataType: string): Promise<any> 
```

Example:
```
import * as foopipes from "foopipes";
import ICallContext = foopipes.ICallContext;
import IResult = foopipes.IResult;
import ProcessJsonResult = foopipes.ProcessJsonResult;

export async function AsyncTest(event: Object, context: ICallContext): Promise<IResult> {
    var r = await foopipes.bindValue(context, "#{file:myvalue}");
    return new ProcessJsonResult(event);
}

```

## Working with Streams ##

You can both read and return binary results as streams.

```
import * as foopipes from "foopipes";
import ICallContext = foopipes.ICallContext;
import IResult = foopipes.IResult;
import ProcessStreamResult = foopipes.ProcessStreamResult;
import IStreamEvent = foopipes.IStreamEvent;

var gm = require('gm').subClass({imageMagick: true});

export function ImageResize(event: IStreamEvent, context: ICallContext): Promise<IResult>
{
  return new Promise<IResult>((resolve, reject) => {
        const r = new ProcessStreamResult(context);

        // Invoke the 'gm' NPM module, and have it pipe the resulting image data back
        let transform = gm(event.stream,'img.png')
            .resize(50, 50);

        const str = transform.stream();

        str.pipe(r.stream).on('finish', ()=>resolve(r));
    });  
}
```
