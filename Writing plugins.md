# Writing Plugins #

Plugins are written in .NET Core and must target framework netcoreapp1.1

Write a class that implements the interface ```Foopipes.Abstractions.IPlugin```

```
#!csharp
using Foopipes.Abstractions;
using Microsoft.AspNetCore.Builder;

    public class PluginStartup : IPlugin
    {
        public void ConfigureServices(IPluginBuilder pluginBuilder)
        {
            // Register my types in ioc container
            pluginBuilder.AddMyPlugin();
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseMyPlugin();
        }
    }
```


Build and publish with
    dotnet publish -o ..\Plugins\MyPlugin

Load the plugin with 
```
#!json
    "plugins": [
      {
        "assemblyName": "MyPlugin",
        "path": "plugins\\myplugin",
        "filename": "MyPlugin.dll"
      }
    ]
```
