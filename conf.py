html_theme_options = {
    'collapse_navigation': False,
    'display_version': False,
    'navigation_depth': 3,
    'canonical_url': 'http://docs.foopipes.com/'
}