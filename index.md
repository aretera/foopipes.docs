# Welcome to Foopipes #

## What is Foopipes ##
Foopipes is a lightweight integration engine for easy retrieval of data across sources, do data transformations and ingestion of data into various data stores or pushed to other external services. Foopipes can also expose its own endpoints and act as a service on its own.

It uses a message based workflow-like architecture for reliable operation with error queues and retries.

It runs in Docker and is configuration driven using yaml or json configuration files.

Data transformation is performed using C# scripts or Node.js modules. Scripts can either be .csx files or inlined in the configuration file. Node.js modules are written in Javascript or transpiled from Typescript or your language of choice.

Read More - [What is Foopipes](What Is.md)

![What is Foopipes](WhatIsFoopipes.svg)

## Guides ##
* [Getting started](Getting started.md)
* [Using the Docker images](Using the Docker images.md)
* [Configuration Basics](Config.md)
* [Some Examples with explanation](Examples.md)

## Examples ##
A few examples with explanation can be found here under [Examples](Examples.md).

Also check out the [Foopipes.Examples Repository](https://github.com/AreteraAB/Foopipes.Examples) for 
some out-of-the-box working examples.

* Empty project template
* Contentful CMS as-a-service without service dependency
* Hugo static website generator with dynamic content from Contentful - [Contentful2Hugo](https://github.com/AreteraAB/Contentful2Hugo)
* Searchable Facebook and Twitter feed (coming soon)
* EpiServer CMS website in Docker with Node.js (coming soon)
* Wordpress without php (coming soon)
